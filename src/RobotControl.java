/**
 * Method for processing arm is divided into 3 stages:
 * 1. Find where the next block needs to go
 * 2. Calculate how to get there.
 * 3. Execute movement.
 * @author Luke Larobina
 *
 */
class RobotControl
{
    private Robot r;
    public static StringBuilder sb;
    
    // constant for shifting bar array to the columns
    public static final int BAR_ARRAY_SHIFT = 3;
    
    // robot arms
    private int rh;
    private int rw;
    private int rd;
    
    // blocks stacked in cols 1 and 2. Used for part C.
    private int blocksInCol1 = 0;
    private int blocksInCol2 = 0;
    
    public RobotControl(Robot r)
    {
        this.r = r;
    }

    public void control(int barHeights[], int blockHeights[])
    {
        run(barHeights, blockHeights);

    }

    public void run(int barHeights[], int blockHeights[])
    {
        
        // we need three things:
        // 1. to determine where the next block needs to go.
        // 2. to determine the height / width / depth that the robot needs to be.
        // 3. to move the arm until it goes there.
        
        // need to keep track of how many bars have been stacked. This checks off bars that have blocks stacked on them.
        boolean[] stackedBars = new boolean[barHeights.length];
        
        // keep track of which block needs to be stacked next.
        int stackedBlocks = blockHeights.length - 1;
        
        // reset blocks in cols 1 and 2
        blocksInCol1 = 0;
        blocksInCol2 = 0;
        
        // init robot
        rh = 2;
        rw = 1;
        rd = 0;
        
        while (stackedBlocks >= 0)
        {
            
            // determine where we need to go next
            int nextCol = 0;
            if (blockHeights[stackedBlocks] == 1)
            {
                nextCol = 1;
            }
            else if (blockHeights[stackedBlocks] == 2)
            {
                nextCol = 2;
            }
            else
            {
                // next available bar. This is actually more complicated than getting the smallest!
                nextCol = getSmallestFreeBar(stackedBars, barHeights, blockHeights, stackedBlocks);
            }
            
            // get max height
            int maxHeight = checkMaxHeight(blockHeights, barHeights, stackedBlocks, stackedBars, nextCol, false);
            
            // determine the height that the crane needs to be to pick up a block
            int neededPickHeight = 0;
            for (int i = stackedBlocks; i >= 0; i--)
            {
                neededPickHeight += blockHeights[i];
            }
            int neededDepthPick = maxHeight - 1 - neededPickHeight;
            
            // cheeky drop ;) If the height can be lowered slightly instead of depth, do that to save moves.
            int cheekyDrop = checkMaxHeight(blockHeights, barHeights, stackedBlocks, stackedBars, nextCol, true);
            
            // determine the height that the crane needs to be to drop the block
            int neededDropHeight;
            if (nextCol == 1)
            {
                neededDropHeight = blocksInCol1 + 1;
            }
            else if (nextCol == 2)
            {
                neededDropHeight = blocksInCol2 * 2 + 2;
            }
            else
            {
                neededDropHeight = barHeights[nextCol - BAR_ARRAY_SHIFT] + 3;
            }
            int neededDepthDrop = cheekyDrop - 1 - neededDropHeight;
            
            // begin movement
            setDepth(0);                // make sure d is retracted! This prevents conflicts later on.
            setHeight(maxHeight);       // set height - setting this as low as possible can save moves when we move d!
            setWidth(10);               // extend to blocks (col 10)
            setDepth(neededDepthPick);  // lower arm to the block
            r.pick();                   // pick up
            setDepth(0);                // raise again to prevent collision
            setWidth(nextCol);          // go to destination
            setHeight(cheekyDrop);      // lower height arm if possible to save on moves
            setDepth(neededDepthDrop);  // lower depth arm
            r.drop();                   // drop
            
            // update block status
            stackedBlocks--;
            if (nextCol == 1)
            {
                blocksInCol1++;
            }
            else if (nextCol == 2)
            {
                blocksInCol2++;
            }
            else
            {
                stackedBars[nextCol - BAR_ARRAY_SHIFT] = true;
            }
            
        }
        
    }
    
    /**
     * Easy method for setting height.
     * @param height
     */
    private void setHeight(int height)
    {
        while (rh != height)
        {
            if (rh < height)
            {
                r.up();
                rh++;
            }
            else if (rh > height)
            {
                r.down();
                rh--;
            }
        }
    }
    
    /**
     * Easy method for setting width.
     * @param width
     */
    private void setWidth(int width)
    {
        while (rw != width)
        {
            if (rw < width)
            {
                r.extend();
                rw++;
            }
            else if (rw > width)
            {
                r.contract();
                rw--;
            }
        }
    }
    
    /**
     * Easy method for setting depth.
     * @param depth
     */
    private void setDepth(int depth)
    {
        while (rd != depth)
        {
            if (rd < depth)
            {
                r.lower();
                rd++;
            }
            else if (rd > depth)
            {
                r.raise();
                rd--;
            }
        }
    }
    
    /**
     * Returns the column of the smallest free bar. This is used to stack the next block onto.
     * @param stackedBars
     * @param barHeights
     * @param blockHeights
     * @param stackedBlocks
     * @return Column position of the next bar to stack.
     */
    private int getSmallestFreeBar(boolean[] stackedBars, int[] barHeights, int[] blockHeights, int stackedBlocks)
    {
        // check how many blocks need to be placed on bars, including this one.
        int remaining3Blocks = 0;
        boolean only3Blocks = true;
        for (int i = stackedBlocks; i >= 0; i--)
        {
            if (blockHeights[i] == 3)
            {
                remaining3Blocks += 1;
            }
            // are there only bars remaining? If so, we can just place in the right-most spot! Otherwise, place normally
            else
            {
                only3Blocks = false;
            }
        }
        int skips = 0;  // for tracking how many bars have been skipped over due to this check.
        int skips3Blocks = 0;   // another skipping tracker for the specific case of making a shortcut work.
        int sameHeightBars = 0;
        
        // get max bar height for checking ideal placements
        int maxBarHeight = 0;
        for (int max : barHeights)
        {
            if (max > maxBarHeight)
            {
                maxBarHeight = max;
            }
        }
        
        int smallestBarPos = 0;
        int smallestBarHeight = 10;
        for (int i = stackedBars.length - 1; i >= 0; i--)
        {
            if (!stackedBars[i])
            {
                // if we only have bars remaining, we place in the rightmost spot.
                if (only3Blocks)
                {
                    if (remaining3Blocks == skips3Blocks + 1)
                    {
                        smallestBarPos = i + BAR_ARRAY_SHIFT;
                        smallestBarHeight = barHeights[i];
                    }
                    
                    skips3Blocks++;
                }
                else
                {
                    
                    // check if we have found a bar which will never interfere with the system. Place it first!
                    if (barHeights[i] <= maxBarHeight - 3)
                    {
                        smallestBarPos = i + BAR_ARRAY_SHIFT;
                        break;
                    }
                    
                    // check if placing here will cause a collision. If so, go to next bar.
                    if (barHeights[i] == 7 && remaining3Blocks > skips + 1)
                    {
                        skips++;
                        continue;
                    }
                    
                    // get the smallest
                    if (barHeights[i] < smallestBarHeight)  // note this implies that the right-most bar gets stacked first
                    {
                        smallestBarPos = i + BAR_ARRAY_SHIFT;
                        smallestBarHeight = barHeights[i];
                        sameHeightBars = 0;
                    }
                    // if this block is the same as one to the right, and there are still more blocks left, then we
                    // check to see how many blocks are left and move over to the left to make the next few 3blocks
                    // faster to place.
                    else if (barHeights[i] == smallestBarHeight)
                    {
                        sameHeightBars++;
                        if (remaining3Blocks == sameHeightBars + 1)
                        {
                            smallestBarPos = i + BAR_ARRAY_SHIFT;
                            break;
                        }
                    }
                    
                }
            }
            
        }
        
        return smallestBarPos;
    }
    
    /**
     * Used to check the height that the arm needs to be set. Depth is calculated afterwards. The main purpose of this
     * method is to optimise the arm height and keep it as low as possible.
     * @param blockHeights
     * @param barHeights
     * @param stackedBlocks
     * @param stackedBars
     * @param nextCol
     * @param carryingBlock
     * @return
     */
    private int checkMaxHeight(int[] blockHeights, int[] barHeights, int stackedBlocks, boolean[] stackedBars,
                               int nextCol, boolean carryingBlock)
    {
     // determine maximum height - do this at every step to determine the least possible moves!
        int[] allHeightChecks = new int[4];
        
        // check 1: stacked blocks in col 10.
        int check1Start = stackedBlocks;
        if (carryingBlock)
        {
            check1Start -= 1;
        }
        for (int i = check1Start; i >= 0; i--)
        {
            allHeightChecks[0] += blockHeights[i];
        }
        
        // check 2: bar columns
        for (int i = 0; i < barHeights.length; i++)
        {
            int check = barHeights[i];
            
            // has been stacked
            if (stackedBars[i])
            {
                check += 3;
            }
            
            // if we are passing over it, consider the height of the next object
            if (nextCol <= i + BAR_ARRAY_SHIFT)
            {
                check += blockHeights[stackedBlocks];
            }
            
            if (check > allHeightChecks[1])
            {
                allHeightChecks[1] = check;
            }
        }
        
        // check 3: col 1
        allHeightChecks[2] = blocksInCol1;
        if (nextCol == 1)   // if we are stacking here, then add 1 to account for the next block
        {
            allHeightChecks[2] += 1;
        }
        
        // check 4: col 2
        allHeightChecks[3] = blocksInCol2 * 2;
        if (nextCol <= 2)   // if we are passing over, add next block height
        {
            allHeightChecks[3] += blockHeights[stackedBlocks];
        }
        
        // last step: compare
        int maxHeight = 0;  // default, max possible height
        for (int heightCheck : allHeightChecks)
        {
            if (heightCheck > maxHeight)
            {
                maxHeight = heightCheck;
            }
        }
        maxHeight += 1; // previous calcs worked off an index of 0. Shift index to 1.
        if (maxHeight < 2)
        {
            maxHeight = 2;
        }
        else if (maxHeight > 13)
        {
            maxHeight = 13;
        }
        
        return maxHeight;
    }
    
}
